<?php
declare(strict_types=1);

namespace Merkle\Sml\Helper;

use Exception;
use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * Class ProductAttributeHelper
 * This class provides helper methods for getting the values of attributes from products
 *
 * @api
 * @since 1.0.0
 */
class ProductAttributeHelper
{
    /**
     * @param ProductResourceModel $productResourceModel
     * @param LoggerInterface $logger
     */
    public function __construct(
        private readonly ProductResourceModel $productResourceModel,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * Get the raw attribute value(s) from the database
     *
     * @param int $entityId Product ID
     * @param string|int $attribute Attribute code or ID
     * @param Store|int|null $store Optional Store or store ID; if omitted, the current store ID is used
     * @return string|null The raw attribute value if present; otherwise, `null`
     * @since 1.0.0
     */
    public function getAttributeRawValue(int $entityId, string|int $attribute, Store|int|null $store = null): ?string
    {
        $attributeRawValue = $this->productResourceModel->getAttributeRawValue($entityId, $attribute, $store);

        if (empty($attributeRawValue)) {
            return null;
        }

        return (string)$attributeRawValue;
    }

    /**
     * Get the raw attribute value(s) from the database and return as an array if it has value(s)
     *
     * @param int $entityId Product ID
     * @param string|int $attribute Attribute code or ID
     * @param Store|int|null $store Optional Store or store ID; if omitted, the current store ID is used
     * @return array|null The raw attribute value(s) in an array if present; otherwise, `null`
     * @since 1.0.0
     */
    public function getAttributeRawValueAsArray(
        int $entityId,
        string|int $attribute,
        Store|int|null $store = null
    ): ?array {
        $attributeRawValue = $this->getAttributeRawValue($entityId, $attribute, $store);

        if (empty($attributeRawValue)) {
            return null;
        }

        return explode(',', $attributeRawValue);
    }

    /**
     * Get the attribute(s) text from the database based on the raw attribute value(s)
     *
     * @param int $entityId Product ID
     * @param string|int $attribute Attribute code or ID
     * @param Store|int|null $store Optional Store or store ID; if omitted, the current store ID is used
     * @return string|null The attribute(s) text if present; otherwise, `null`
     * @since 1.0.0
     */
    public function getAttributeText(int $entityId, string|int $attribute, Store|int|null $store = null): ?string
    {
        $attributeRawValue = $this->getAttributeRawValue($entityId, $attribute, $store);

        if (empty($attributeRawValue)) {
            return null;
        }

        try {
            $attribute = $this->productResourceModel->getAttribute($attribute);
        } catch (Exception $e) {
            $this->logger->critical(
                $e->getMessage(),
                [
                    'exception' => $e,
                    'method' => __METHOD__,
                ]
            );

            return null;
        }

        if (!$attribute) {
            return null;
        }

        try {
            $source = $attribute->getSource();
        } catch (Exception $e) {
            $this->logger->critical(
                $e->getMessage(),
                [
                    'exception' => $e,
                    'method' => __METHOD__,
                ]
            );

            return null;
        }

        $optionText = $source->getOptionText($attributeRawValue);

        if (!$optionText) {
            return null;
        }

        if (is_array($optionText)) {
            return implode(',', $optionText);
        }

        return (string)$optionText;
    }

    /**
     * Get the attribute(s) text from the database and return as an array if it has value(s)
     *
     * @param int $entityId Product ID
     * @param string|int $attribute Attribute code or ID
     * @param Store|int|null $store Optional Store or store ID; if omitted, the current store ID is used
     * @return array|null The attribute(s) text in an array if present; otherwise, `null`
     * @since 1.0.0
     */
    public function getAttributeTextAsArray(
        int $entityId,
        string|int $attribute,
        Store|int|null $store = null
    ): ?array {
        $attributeText = $this->getAttributeText($entityId, $attribute, $store);

        if (empty($attributeText)) {
            return null;
        }

        return explode(',', $attributeText);
    }
}
