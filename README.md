# Merkle_Sml

The standard Magento library module

This module contains a variety of classes to help deal with logic that is needed on the majority of Magento projects. It
contains logic that can be re-used to cut down on boilerplate from project to project.

## Installation

TODO

## Usage

### Helper

#### AbstractSystemHelper

An abstract helper exists for dealing with system configuration values in your custom module(s). This class should be
extended with a custom class per module that provides methods to access that module's configuration values.
An example class can be found at `/docs/examples/Helper/SystemHelper.php`.

#### ProductAttributeHelper

This helper provides a number of methods for getting the values and/or text of attributes for a specified product.

##### getAttributeRawValue()

Given a product ID and attribute code (and optional store scope), this will return the raw attribute value(s) for that
attribute of that product. It returns this value as a string if it exists; otherwise, it returns `null`.

##### getAttributeRawValueAsArray()

Works the same as `getAttributeRawValue()`, but returns an array instead of a string if the value(s) is found;
otherwise, it still returns `null`. This can be useful when working with an attribute that may have multiple values,
like multi-selects.

##### getAttributeText()

Get the text value(s) rather than the raw value(s). This method will call `getAttributeRawValue()`. If that returns a
value(s), this will return the text of that value(s) as a string; otherwise, it returns `null`.

##### getAttributeTextAsArray()

Works the same as `getAttributeText()`, but returns an array instead of a string.
