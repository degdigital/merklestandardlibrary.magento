<?php
/** @noinspection DuplicatedCode */
declare(strict_types=1);

namespace Merkle\Sml\Test\Unit\Helper;

use Exception;
use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Merkle\Sml\Helper\ProductAttributeHelper;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ProductAttributeHelperTest extends TestCase
{
    /**
     * @var ProductAttributeHelper
     */
    private ProductAttributeHelper $object;

    /**
     * @var ProductResourceModel|MockObject
     */
    private ProductResourceModel|MockObject $productResourceModelMock;

    /**
     * @var LoggerInterface|MockObject
     */
    private LoggerInterface|MockObject $loggerMock;

    /**
     * @var AbstractAttribute|MockObject
     */
    private AbstractAttribute|MockObject $attributeMock;

    /**
     * @var AbstractSource|MockObject
     */
    private AbstractSource|MockObject $sourceMock;

    protected function setUp(): void
    {
        $this->productResourceModelMock = $this->createMock(ProductResourceModel::class);
        $this->loggerMock = $this->createMock(LoggerInterface::class);

        $this->attributeMock = $this->createMock(AbstractAttribute::class);

        $this->sourceMock = $this->createMock(AbstractSource::class);

        $this->object = new ProductAttributeHelper(
            $this->productResourceModelMock,
            $this->loggerMock
        );
    }

    public function testItIsInitializable()
    {
        $this->assertInstanceOf(ProductAttributeHelper::class, $this->object);
    }

    public function testGetAttributeRawValueReturnsNullForAnUnknownAttribute()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn([]);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertNull($this->object->getAttributeRawValue(1, 'unknown'));
    }

    public function testGetAttributeRawValueReturnsAStringForAKnownAttribute()
    {
        $attributeRawValue = '31,142';

        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn($attributeRawValue);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertSame($attributeRawValue, $this->object->getAttributeRawValue(1, 'known'));
    }

    public function testGetAttributeRawValueAsArrayReturnsNullForAnUnknownAttribute()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn([]);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertNull($this->object->getAttributeRawValueAsArray(1, 'unknown'));
    }

    public function testGetAttributeRawValueAsArrayReturnsAnArrayForAKnownAttribute()
    {
        $attributeRawValue = '31,142';

        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn($attributeRawValue);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertSame(explode(',', $attributeRawValue), $this->object->getAttributeRawValueAsArray(1, 'known'));
    }

    public function testGetAttributeTextReturnsNullForAnUnknownAttribute()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn([]);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertNull($this->object->getAttributeText(1, 'unknown'));
    }

    public function testGetAttributeTextReturnsNullIfGetAttributeThrowsAnException()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31,142');
        $this->productResourceModelMock->method('getAttribute')
            ->willThrowException(new Exception('Testing...'));

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->loggerMock->expects($this->once())
            ->method('critical');

        $this->assertNull($this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextReturnsNullIfGetAttributeReturnsFalse()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31,142');
        $this->productResourceModelMock->method('getAttribute')
            ->willReturn(false);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertNull($this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextReturnsNullIfGetSourceThrowsAnException()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31,142');
        $this->productResourceModelMock->method('getAttribute')
            ->willReturn($this->attributeMock);

        $this->attributeMock->method('getSource')
            ->willThrowException(new Exception('Testing...'));

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->loggerMock->expects($this->once())
            ->method('critical');

        $this->attributeMock->expects($this->once())
            ->method('getSource');

        $this->assertNull($this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextReturnsNullIfGetOptionTextReturnsFalse()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31,142');
        $this->productResourceModelMock->method('getAttribute')
            ->willReturn($this->attributeMock);

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->attributeMock->method('getSource')
            ->willReturn($this->sourceMock);

        $this->sourceMock->method('getOptionText')
            ->willReturn(false);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->attributeMock->expects($this->once())
            ->method('getSource');

        $this->sourceMock->expects($this->once())
            ->method('getOptionText');

        $this->assertNull($this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextReturnsAStringIfGetOptionTextReturnsAnArray()
    {
        $optionText = [
            'Wool',
            'Cotton',
            'Fleece',
        ];

        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31,142,144');
        $this->productResourceModelMock->method('getAttribute')
            ->willReturn($this->attributeMock);

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->attributeMock->method('getSource')
            ->willReturn($this->sourceMock);

        $this->sourceMock->method('getOptionText')
            ->willReturn($optionText);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->attributeMock->expects($this->once())
            ->method('getSource');

        $this->sourceMock->expects($this->once())
            ->method('getOptionText');

        $this->assertSame(implode(',', $optionText), $this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextReturnsAStringIfGetOptionTextReturnsAString()
    {
        $optionText = 'Wool';

        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn('31');
        $this->productResourceModelMock->method('getAttribute')
            ->willReturn($this->attributeMock);

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->attributeMock->method('getSource')
            ->willReturn($this->sourceMock);

        $this->sourceMock->method('getOptionText')
            ->willReturn($optionText);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');
        $this->productResourceModelMock->expects($this->once())
            ->method('getAttribute');

        $this->attributeMock->expects($this->once())
            ->method('getSource');

        $this->sourceMock->expects($this->once())
            ->method('getOptionText');

        $this->assertSame($optionText, $this->object->getAttributeText(1, 'known'));
    }

    public function testGetAttributeTextAsArrayReturnsNullForAnUnknownAttribute()
    {
        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn([]);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertNull($this->object->getAttributeTextAsArray(1, 'unknown'));
    }

    public function testGetAttributeTextAsArrayReturnsAnArrayForAKnownAttribute()
    {
        $attributeText = 'Wool,Cotton,Fleece';

        $this->productResourceModelMock->method('getAttributeRawValue')
            ->willReturn($attributeText);

        $this->productResourceModelMock->expects($this->once())
            ->method('getAttributeRawValue');

        $this->loggerMock->expects($this->never())
            ->method('critical');

        $this->assertSame(explode(',', $attributeText), $this->object->getAttributeRawValueAsArray(1, 'known'));
    }
}
