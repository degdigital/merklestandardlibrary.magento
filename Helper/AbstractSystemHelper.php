<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace Merkle\Sml\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\InvalidArgumentException;
use Magento\Store\Model\ScopeInterface;

abstract class AbstractSystemHelper
{
    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        protected readonly ScopeConfigInterface $scopeConfig
    ) {
    }

    /**
     * Retrieve config value by path and scope
     *
     * @param string $path
     * @param string|int|null $scopeCode
     * @param string $scopeType
     * @return mixed
     * @throws InvalidArgumentException
     */
    protected function getValue(
        string $path,
        string|int|null $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): mixed {
        $this->validate($scopeCode, $scopeType);

        return $this->scopeConfig->getValue($path, $scopeType, $scopeCode);
    }

    /**
     * Retrieve config flag by path and scope
     *
     * @param string $path
     * @param string|int|null $scopeCode
     * @param string $scopeType
     * @return bool
     * @throws InvalidArgumentException
     */
    protected function isSetFlag(
        string $path,
        string|int|null $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): bool {
        $this->validate($scopeCode, $scopeType);

        return $this->scopeConfig->isSetFlag($path, $scopeType, $scopeCode);
    }

    /**
     * Validate `$scopeCode` and `$scopeType` usage
     *
     * @param string|int|null $scopeCode
     * @param string $scopeType
     * @return void
     * @throws InvalidArgumentException
     */
    protected function validate(
        string|int|null $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): void {
        if ($scopeType === ScopeConfigInterface::SCOPE_TYPE_DEFAULT && $scopeCode) {
            throw new InvalidArgumentException(
                __('When using the default `$scopeType`, `$scopeCode` must be omitted or be `null` or `0`')
            );
        }
    }
}
