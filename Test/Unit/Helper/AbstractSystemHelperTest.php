<?php
declare(strict_types=1);

namespace Merkle\Sml\Test\Unit\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\InvalidArgumentException;
use Merkle\Sml\Helper\AbstractSystemHelper;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionMethod;

class AbstractSystemHelperTest extends TestCase
{
    /**
     * @var AbstractSystemHelper|MockObject
     */
    private AbstractSystemHelper|MockObject $object;

    /**
     * @var ScopeConfigInterface|MockObject
     */
    private ScopeConfigInterface|MockObject $scopeConfigMock;

    protected function setUp(): void
    {
        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);

        $this->object = $this->getMockForAbstractClass(
            AbstractSystemHelper::class,
            [
                $this->scopeConfigMock,
            ]
        );
    }

    public function testGetValueReturnsAppropriateValue()
    {
        $value = 'Testing...';

        $this->scopeConfigMock->method('getValue')
            ->willReturn($value);

        $this->scopeConfigMock->expects($this->once())
            ->method('getValue');

        $getValue = static::getTestMethod('getValue');

        /** @noinspection PhpUnhandledExceptionInspection */
        $this->assertSame($value, $getValue->invokeArgs($this->object, ['test', 1]));
    }

    public function testIsSetFlagReturnsAppropriateBool()
    {
        $isSet = true;

        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->scopeConfigMock->method('isSetFlag')
            ->willReturn($isSet);

        $this->scopeConfigMock->expects($this->once())
            ->method('isSetFlag');

        $isSetFlag = static::getTestMethod('isSetFlag');

        /** @noinspection PhpConditionAlreadyCheckedInspection */
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->assertSame($isSet, $isSetFlag->invokeArgs($this->object, ['test', 1]));
    }

    public function testGetValueThrowsExceptionWhenUsingDefaultScopeTypeAndPassingScopeCode()
    {
        $this->expectException(InvalidArgumentException::class);

        $getValue = static::getTestMethod('getValue');

        /** @noinspection PhpUnhandledExceptionInspection */
        $getValue->invokeArgs($this->object, ['test', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT]);
    }

    public function testIsSetFlagThrowsExceptionWhenUsingDefaultScopeTypeAndPassingScopeCode()
    {
        $this->expectException(InvalidArgumentException::class);

        $isSetFlag = static::getTestMethod('isSetFlag');

        /** @noinspection PhpUnhandledExceptionInspection */
        $isSetFlag->invokeArgs($this->object, ['test', 1, ScopeConfigInterface::SCOPE_TYPE_DEFAULT]);
    }

    protected static function getTestMethod(string $method): ReflectionMethod
    {
        $reflectionClass = new ReflectionClass(AbstractSystemHelper::class);

        /** @noinspection PhpUnhandledExceptionInspection */
        $reflectionMethod = $reflectionClass->getMethod($method);

        /** @noinspection PhpExpressionResultUnusedInspection */
        $reflectionMethod->setAccessible(true);

        return $reflectionMethod;
    }
}
