# Changelog

All notable changes to this module will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Merkle_Sml

The standard Magento library module

This module contains a variety of classes to help deal with logic that is needed on the majority of Magento projects. It
contains logic that can be re-used to cut down on boilerplate from project to project.

## [1.0.0] - 2023-08-09

### Added

- Initial module
  files (https://experienceleague.adobe.com/docs/commerce-learn/tutorials/backend-development/create-module.html?lang=en)
- `\Merkle\Sml\Helper\AbstractSystemHelper`
- `\Merkle\Sml\Helper\ProductAttributeHelper`
