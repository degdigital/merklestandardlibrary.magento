<?php
declare(strict_types=1);

namespace Merkle\CustomModule\Helper;

use Magento\Store\Model\ScopeInterface;
use Merkle\Sml\Helper\AbstractSystemHelper;

/**
 * Class SystemHelper
 * This class will provide an API for dealing with the system config values for the `Merkle_CustomModule` module.
 * Ideally, each module will have its own and a single one to deal with its specific configuration options.
 * Try to establish a naming convention per project and stick to it. This makes it easy to know where to find a
 * module's system config class. `/Helper/SystemHelper.php` is suitable for most projects. Be sure to extend the
 * abstract helper to ensure you can use the helper methods and the validation is used.
 */
class SystemHelper extends AbstractSystemHelper
{
    /**
     * Using constants for the XML path to each configuration value is a good practice to use. It allows you to see the
     * different available fields at a quick glass. In most situations, these can be marked `private` as they don't
     * need to be used directly. You'll use the class's `public` methods instead. There are valid reasons to mark these
     * as `public`. Just keep in mind, if you do that, they're now part of the class's API, and changes to them may
     * break client code that relies on the old values.
     * Try to follow a naming conventions to differentiate these `const`s from other ones the class may declare.
     * `XML_PATH_` followed by the individual field name is a good convention for most projects.
     */
    private const XML_PATH_ENABLED = 'merkle_custommodule/general/enabled';

    private const XML_PATH_OTHER_FIELD = 'merkle_custommodule/general/other_field';

    /**
     * Create individual `public` methods for retrieving each individual config field value.
     * In most instances, you'll want to keep the switched order of `$scopeCode` and `$scopeType` as seen below rather
     * than the order they fall in in `\Magento\Framework\App\Config\ScopeConfigInterface`. This allows you to default
     * to the most specific scope and omit all values for the current store or pass a single store ID to get the value
     * for a specific, non-current store. This still allows you to change the scope to website or default when
     * necessary. There are times when using a less specific scope for your project makes sense. If you're dealing with
     * a multi-site instance, but each website only contains a single store and config values are only set at the
     * website or default level, changing the `$scopeType` to website is perfectly valid. If dealing with a
     * single-store instance, using the default scope is valid. In general, you should use the most specific
     * scope appropriate for your project and the module's configuration settings. Inside these methods, call the
     * appropriate helper methods and pass the XML path, the `$scopeCode`, and the `$scopeType`. When you're expecting
     * a `boolean` value, `isSetFlag()` should be used. For non-boolean values, `getValue()` should be used. If you try
     * to retrieve a value from either helper method and you're using the default scope, `$scopeCode` must be omitted
     * (or empty (i.e., `null`, `0`, etc...); otherwise, an exception is thrown as passing a `$scopeCode` means you
     * likely don't want the default value. If you omit the `$scopeCode` and you're getting a value for the store or
     * website scope, Magento will use the current store (or website) ID automatically. That means when you want the
     * value for the current site, you can simply call `isEnabled()`, for example, without any additional parameters.
     * If a value isn't set for the scope you've passed, Magento will start working it's way up in the inheritance
     * tree, trying website after store, then finally default after website.
     */
    public function isEnabled(string|int|null $scopeCode = null, string $scopeType = ScopeInterface::SCOPE_STORE): bool
    {
        return $this->isSetFlag(static::XML_PATH_ENABLED, $scopeType, $scopeCode);
    }

    /**
     * If you're expecting a certain type to be returned, adding in some checks like below and declaring a return type
     * can help make your code more reliable and limit the number of checks the calling code has to perform when
     * retrieving a value.
     */
    public function getOtherField(
        string|int|null $scopeCode = null,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): ?string {
        $value = $this->getValue(static::XML_PATH_OTHER_FIELD, $scopeCode, $scopeType);

        if (!$value) {
            return null;
        }

        return (string)$value;
    }
}
